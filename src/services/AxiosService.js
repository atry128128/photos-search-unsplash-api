//const axios = require('axios');
import axios from 'axios';
const URLapi = 'https://api.unsplash.com';
const URL = 'https://unsplash.com';
const clientID = 'hlZKFHMRYtbMsFmnrx1MTcgJEJne7BrNfr3IPZFtwd0';

export const fetchImages = (inputValue) => {
    return inputValue && axios.get(`${URLapi}/search/photos?client_id=${clientID}&query=${inputValue}`);
}
export const fetchSuggested = (word) => {
    return axios.get(`${URL}/nautocomplete/${word}`, {
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
    })
}
