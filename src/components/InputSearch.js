import React, {useState} from "react";
import {useHistory} from "react-router-dom";
import {fetchSuggested} from "../services/AxiosService";
import '../assets/styles/input-component.css'

const InputSearch = ({inputValue, setInputValue, sumbitCallback}) => {
    const [suggested, setSuggested] = useState([]);

    const [firstRun, setFirstRun] = useState(true);
    let history = useHistory();

    React.useEffect(() => {
        (async function () {
            if ( !firstRun && inputValue && inputValue.length >= 3) {
                const responseSuggested = await fetchSuggested(inputValue);
                setSuggested(responseSuggested.data.autocomplete);
            } else{
                setSuggested([]);

            }
        })();
        setFirstRun(false);
    }, [inputValue]);

    const submit = e => {
        e.preventDefault();
        history.push({
            pathname: '/search',
            search: '?q=' + inputValue,
        })
        sumbitCallback && sumbitCallback();
        setFirstRun(true);
        setSuggested([])
    }

    return (
        <>

            <input
                autoComplete="off"
                id='input'
                type='text'
                placeholder={'Search free high-resolution photos'}
                value={inputValue}
                onKeyPress={(e) => {
                    if (e.key==='Enter')
                        submit(e);
                }}
                onChange={(e) => setInputValue(e.target.value)}
            />
            {inputValue && inputValue.length >=3 &&
            <ul>{suggested.map((suggest, key) =>
                <li
                    key={key}
                    onClick={(e) => {setInputValue(suggest.query); submit(e);}}>
                    {suggest.query}
                </li>)}
            </ul>}
        </>
    )
}
export default InputSearch;