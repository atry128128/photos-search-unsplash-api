import {Modal} from 'react-bootstrap'

const ModalPhoto = ({show, handleClose, infoPhoto}) => (
        <Modal show={show} onHide={handleClose}>
            <Modal.Dialog>
                <Modal.Body>
                    <img src={infoPhoto.urls.small} alt={''}/>
                </Modal.Body>
            </Modal.Dialog>
        </Modal>
)
export default ModalPhoto