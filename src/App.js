import React, {useState} from 'react';
import {
    BrowserRouter as Router,
    Route, Link, Switch
} from "react-router-dom";
import SearchPage from "./pages/SearchPage";
import HomePage from "./pages/HomePage";

const App = () => {

    return (
        <Router>
            <Switch>
                <Route exact path='/' children={<HomePage/>}/>
                <Route exact path='/search' children={<SearchPage/>}/>
            </Switch>
        </Router>
    )
}

export default App;
