import React, {useState} from 'react';
import InputSearch from "../components/InputSearch";
import '../assets/styles/homepage.css'

const HomePage = () => {
    const [inputValue, setInputValue] = useState('');
    return (
        <div className={'homepage-background'}>
            <div className={'homepage-items-container'}>
                <div className={'homepage-title'}>Unsplash</div>
                The internet’s source of freely-usable images.
                <br/>
                Powered by creators everywhere.
                <br/>
                <InputSearch inputValue={inputValue} setInputValue={setInputValue}/>
            </div>
        </div>
    )
}
export default HomePage;