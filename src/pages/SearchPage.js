import React, {useEffect, useState} from 'react';
import {fetchImages} from "../services/AxiosService";
import InputSearch from "../components/InputSearch";
import ModalPhoto from "../components/ModalPhoto";

const SearchPage = () => {
    const [inputValue, setInputValue] = useState((new URLSearchParams(window.location.search)).get('q'))
    const [photos, setPhotos] = useState([])
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = (photo) => {
        setPhotoToModal(photo)
        setShow(true)
    };

    useEffect(()=>{
     sumbitCallback();
    },[]);

    const sumbitCallback = () => {
        (async function () {
            if (inputValue.length>=3) {
                const responseimages = await fetchImages(inputValue)
                setPhotos(responseimages.data.results)
            }
        })();
    };

    return (
        <>
            <InputSearch inputValue={inputValue} setInputValue={setInputValue} sumbitCallback={sumbitCallback}/>
            <h1>{inputValue}</h1>
            {photos?.map((photo, id) =>
                <img
                    onClick={() => handleShow(photo)}
                    key={id}
                    src={photo.urls.small}
                    alt=''/>)}
            {show && <ModalPhoto
                show={show}
                infoPhoto={photoToModal}
                handleClose={handleClose}/>}
        </>
    )
}
export default SearchPage;
